/*
+ *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.masterdata.uniqueconstraintbylegalentity.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.junit.Rule;
import org.junit.Test;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.weld.test.ParameterCdiTest;
import org.openbravo.base.weld.test.ParameterCdiTestRule;
import org.openbravo.base.weld.test.WeldBaseTest;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.businesspartner.CategoryAccounts;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.pricing.pricelist.PriceList;

public class OBMUCLEEventObserverTest extends WeldBaseTest {
  private static final String MESSAGE_ASSERT = "This test's result";
  private static final String FB_ESPANA_REGION_NORTE = "E443A31992CB4635AFCAEABE7183CE85";
  private static final String FB_ESPANA_REGION_SUR = "DC206C91AA6A4897B44DA897936E0EC3";
  private static final String FB_ESPANA_SA = "B843C30461EA4501935CB1D125C9C25A";
  private static final String FB_US_INC = "2E60544D37534C0B89E765FE29BC0B43";
  private static final String FB_US_EAST_COAST = "7BABA5FF80494CAFA54DEBD22EC46F01";
  private static final String FB_INTERNATIONAL_GROUP = "19404EAD144C49A0AF37D54377CF452D";

  private static final String DEFAULT_VALUE_AND_NAME = "TESTOBJECT";
  private static final String DEFAULT_VALUE_AND_NAME_2 = DEFAULT_VALUE_AND_NAME + "AUX";
  private static final String DEFAULT_PRODUCT = "19857ACFC55D45E2AECAF85B2506C3DB";
  private static final String DEFAULT_BUSINESS_PARTNER = "643236567C05410CBFE77708EA99F89C";
  private static final String DEFAULT_BUSINESS_PARTNER_CATEGORY = "26A1FE81E1EA4A6E8D3517E8232264D1";
  private static final String DEFAULT_PRODUCT_CATEGORY = "291B401A38354A2C8247DFF0DFBDF4AE";
  private static final String DEFAULT_PRICELIST = "8366EAF1EDF442A98377D74A199084A8";

  private static final String ORGS_IN_DIFFERENT_LEGAL = "Organizations in different Legal Entities -> OK";
  private static final String ORGS_IN_SAME_LEGAL = "Organizations in same Legal Entitiy -> ERROR";
  private static final String ORG_AND_LEGAL = "Organization and its Legal Entitiy -> ERROR";
  private static final String ORGS_IN_SAME_BRANCH = "Organization and its predecessor/successor in hierarchy (same branch) -> ERROR";

  static final List<OBMUCLEEventObserverTestData> PARAMS = Arrays.asList(
      new OBMUCLEEventObserverTestData("01", ORGS_IN_DIFFERENT_LEGAL, FB_ESPANA_REGION_NORTE,
          FB_US_EAST_COAST, false),
      new OBMUCLEEventObserverTestData("02", ORGS_IN_SAME_LEGAL, FB_ESPANA_REGION_NORTE,
          FB_ESPANA_REGION_SUR, true),
      new OBMUCLEEventObserverTestData("03", ORG_AND_LEGAL, FB_ESPANA_REGION_SUR, FB_ESPANA_SA, true),
      new OBMUCLEEventObserverTestData("04", ORGS_IN_SAME_BRANCH, FB_ESPANA_REGION_NORTE,
          FB_INTERNATIONAL_GROUP, true),
      new OBMUCLEEventObserverTestData("05", ORGS_IN_SAME_BRANCH, FB_INTERNATIONAL_GROUP,
          FB_ESPANA_REGION_NORTE, true),
      new OBMUCLEEventObserverTestData("06", ORGS_IN_SAME_LEGAL, FB_ESPANA_REGION_SUR,
          FB_ESPANA_REGION_NORTE, true),
      new OBMUCLEEventObserverTestData("07", ORG_AND_LEGAL, FB_ESPANA_SA, FB_ESPANA_REGION_SUR, true),
      new OBMUCLEEventObserverTestData("08", ORGS_IN_DIFFERENT_LEGAL, FB_US_EAST_COAST,
          FB_ESPANA_REGION_SUR, false),
      new OBMUCLEEventObserverTestData("09", ORGS_IN_DIFFERENT_LEGAL, FB_ESPANA_SA, FB_US_INC, false),
      new OBMUCLEEventObserverTestData("10", ORG_AND_LEGAL, FB_US_INC, FB_US_EAST_COAST, true));

  @Rule
  public ParameterCdiTestRule<OBMUCLEEventObserverTestData> parameterValuesRule = new ParameterCdiTestRule<>(
      PARAMS);

  private @ParameterCdiTest OBMUCLEEventObserverTestData parameter;

  @Test
  public void testInsertProductUCEventObserver() {
    setTestAdminContext();
    Product currentProduct = OBDal.getInstance().get(Product.class, DEFAULT_PRODUCT);
    insertTest(currentProduct);
  }

  @Test
  public void testInsertBusinessPartnerUCEventObserver() {
    setTestAdminContext();
    BusinessPartner currentBusinessPartner = OBDal.getInstance()
        .get(BusinessPartner.class, DEFAULT_BUSINESS_PARTNER);
    insertTest(currentBusinessPartner);
  }

  @Test
  public void testInsertBusinessPartnerCategoryUCEventObserver() {
    setTestAdminContext();
    Category currentBusinessPartnerCategory = OBDal.getInstance()
        .get(Category.class, DEFAULT_BUSINESS_PARTNER_CATEGORY);
    insertTest(currentBusinessPartnerCategory);
  }

  @Test
  public void testInsertProductCategoryUCEventObserver() {
    setTestAdminContext();
    ProductCategory currentProductCategory = OBDal.getInstance()
        .get(ProductCategory.class, DEFAULT_PRODUCT_CATEGORY);
    insertTest(currentProductCategory);
  }

  @Test
  public void testInsertPriceListUCEventObserver() {
    setTestAdminContext();
    PriceList currentPriceList = OBDal.getInstance().get(PriceList.class, DEFAULT_PRICELIST);
    insertTest(currentPriceList);
  }

  @Test
  public void testUpdateProductUCEventObserver() {
    updateTest(Product.ENTITY_NAME, parameter.getOrganization2());
  }

  @Test
  public void testUpdateBusinessPartnerUCEventObserver() {
    updateTest(BusinessPartner.ENTITY_NAME, parameter.getOrganization2());
  }

  @Test
  public void testUpdateBusinessPartnerCategoryUCEventObserver() {
    updateTest(Category.ENTITY_NAME, parameter.getOrganization2());
  }

  @Test
  public void testUpdateProductCategoryUCEventObserver() {
    updateTest(ProductCategory.ENTITY_NAME, parameter.getOrganization2());
  }

  @Test
  public void testUpdatePriceListUCEventObserver() {
    updateTest(PriceList.ENTITY_NAME, parameter.getOrganization2());
  }

  public void insertTest(BaseOBObject currentProduct) {
    genericTest(currentProduct, false);
  }

  private void updateTest(String entityName, String organization) {
    setTestAdminContext();

    BaseOBObject object = getListObjectInOrg(entityName, organization);

    if (object == null) {
      assertEquals(MESSAGE_ASSERT, parameter.isNeededToFail(), parameter.isNeededToFail());
    } else {
      genericTest(object, true);
    }
  }

  private BaseOBObject getListObjectInOrg(String entityName, String organization) {
    Organization org = OBDal.getInstance().get(Organization.class, organization);
    return (BaseOBObject) OBDal.getInstance()
        .createCriteria(entityName)
        .add(Restrictions.eq(Product.PROPERTY_ORGANIZATION, org))
        .setMaxResults(1)
        .uniqueResult();
  }

  private BaseOBObject modifyObject(BaseOBObject object, final Organization organization,
      final boolean isItUpdate, final String valueToUse) {

    BaseOBObject objectTest = null;
    if (isItUpdate) {
      objectTest = object;
    } else {
      objectTest = DalUtil.copy(object, false);
    }

    if (!objectTest.getEntity().getName().equals("PricingPriceList")) {
      objectTest.setValue(Product.PROPERTY_SEARCHKEY, valueToUse);
    }

    objectTest.setValue(Product.PROPERTY_ORGANIZATION, organization);
    objectTest.setValue(Product.PROPERTY_NAME, valueToUse);
    objectTest.setNewOBObject(true);

    OBDal.getInstance().save(objectTest);
    OBDal.getInstance().flush();
    return objectTest;
  }

  private void genericTest(BaseOBObject object, final boolean isItUpdate) {
    boolean failsInConstraint = false;
    BaseOBObject objectToDelete1 = null;
    BaseOBObject objectToDelete2 = null;

    try {
      Organization org1 = OBDal.getInstance().get(Organization.class, parameter.getOrganization1());
      Organization org2 = OBDal.getInstance().get(Organization.class, parameter.getOrganization2());

      objectToDelete1 = modifyObject(object, org1, false, DEFAULT_VALUE_AND_NAME);
      if (isItUpdate) {
        objectToDelete2 = modifyObject(object, org2, false, DEFAULT_VALUE_AND_NAME_2);
        modifyObject(objectToDelete2, org2, true, DEFAULT_VALUE_AND_NAME);
      } else {
        objectToDelete2 = modifyObject(object, org2, false, DEFAULT_VALUE_AND_NAME);
      }

    } catch (OBException e) {
      if (e.toString().contains("@OBMUCLE_NameInUseInsideLegalEntityOrBranch@")
          || e.toString().contains("@OBMUCLE_SearchKeyInUseInsideLegalEntityOrBranch@")) {
        failsInConstraint = true;
      }
      if (e.toString().contains("org.openbravo.base.exception.OBSecurityException")) {
        failsInConstraint = parameter.isNeededToFail();
      }
    }

    assertEquals(MESSAGE_ASSERT, parameter.isNeededToFail(), failsInConstraint);

    removeObjectsAndRelationsCreatedByTrigger(objectToDelete1, objectToDelete2);
  }

  private void removeObjectsAndRelationsCreatedByTrigger(BaseOBObject objectToDelete1,
      BaseOBObject objectToDelete2) {

    OBDal.getInstance().getSession().clear();

    if (objectToDelete1 != null) {
      deleteObjectCreatedInTest(objectToDelete1);
    }
    if (objectToDelete2 != null) {
      deleteObjectCreatedInTest(objectToDelete2);
    }
    OBDal.getInstance().flush();
  }

  private void deleteObjectCreatedInTest(BaseOBObject objectToDelete) {
    BaseOBObject objectToBeDeleted = OBDal.getInstance()
        .get(objectToDelete.getEntityName(), objectToDelete.getId());
    if (objectToBeDeleted.getEntityName().equals(Category.ENTITY_NAME)) {
      deleteRelatedCategoryAccounts(objectToBeDeleted);
    }
    OBDal.getInstance().remove(objectToBeDeleted);
  }

  private void deleteRelatedCategoryAccounts(BaseOBObject objectToBeDeleted) {

    List<BaseOBObject> objectsToDelete = OBDal.getInstance()
        .createCriteria(CategoryAccounts.ENTITY_NAME)
        .add(Restrictions.eq(CategoryAccounts.PROPERTY_BUSINESSPARTNERCATEGORY, objectToBeDeleted))
        .list();

    for (BaseOBObject currentObject : objectsToDelete) {
      OBDal.getInstance().remove(currentObject);
    }
  }
}
