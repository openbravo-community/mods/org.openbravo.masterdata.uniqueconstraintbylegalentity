/*
+ *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.masterdata.uniqueconstraintbylegalentity.test;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class OBMUCLEEventObserverTestData {
  private final String testNumber;
  private final String testDescription;
  private final String organization1;
  private final String organization2;
  private final boolean isNeededToFail;

  public OBMUCLEEventObserverTestData(final String testNumber, final String testDescription,
      final String organization1, final String organization2, final boolean isNeededToFail) {
    this.testNumber = testNumber;
    this.testDescription = testDescription;
    this.organization1 = organization1;
    this.organization2 = organization2;
    this.isNeededToFail = isNeededToFail;
  }

  public String getTestNumber() {
    return testNumber;
  }

  public String getTestDescription() {
    return testDescription;
  }

  public String getOrganization1() {
    return organization1;
  }

  public String getOrganization2() {
    return organization2;
  }

  public boolean isNeededToFail() {
    return isNeededToFail;
  }
}
