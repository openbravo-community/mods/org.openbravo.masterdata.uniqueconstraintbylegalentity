/*
+ *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.masterdata.uniqueconstraintbylegalentity.event;

import javax.enterprise.event.Observes;

import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.pricing.pricelist.PriceList;

class OBMUCLEEventObserver extends EntityPersistenceEventObserver {
  private static Entity[] entities = { ModelProvider.getInstance().getEntity(Product.ENTITY_NAME),
      ModelProvider.getInstance().getEntity(BusinessPartner.ENTITY_NAME),
      ModelProvider.getInstance().getEntity(Category.ENTITY_NAME),
      ModelProvider.getInstance().getEntity(ProductCategory.ENTITY_NAME),
      ModelProvider.getInstance().getEntity(PriceList.ENTITY_NAME) };

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    final BaseOBObject currentObject = event.getTargetInstance();
    final String nameOrValueProperty = getNameOrValue(currentObject);
    final Property modifiedProperty = currentObject.getEntity().getProperty(nameOrValueProperty);
    if (!event.getCurrentState(modifiedProperty).equals(event.getPreviousState(modifiedProperty))) {
      checkCondition(currentObject, nameOrValueProperty);
    }
  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    final BaseOBObject currentObject = event.getTargetInstance();
    final String nameOrValueProperty = this.getNameOrValue(currentObject);
    checkCondition(currentObject, nameOrValueProperty);
  }

  private void checkCondition(BaseOBObject currentObject, String nameOrValueProperty) {

    final Organization org = (Organization) currentObject.get(Product.PROPERTY_ORGANIZATION);
    final Organization legalEntity = org.getLegalEntityOrganization();

    final String legalEntityId = legalEntity != null ? legalEntity.getId() : null;

    if (isUsedInsideSameLegalEntityOrBranch(currentObject.getEntityName(),
        currentObject.get(nameOrValueProperty).toString(), nameOrValueProperty, legalEntityId,
        org.getId())) {
      if (currentObject.getEntityName().equals(PriceList.ENTITY_NAME)) {
        throw new OBException("@OBMUCLE_NameInUseInsideLegalEntityOrBranch@");
      } else {
        throw new OBException("@OBMUCLE_SearchKeyInUseInsideLegalEntityOrBranch@");
      }
    }
  }

  private boolean isUsedInsideSameLegalEntityOrBranch(String entityName, String nameOrValue,
      String nameOrValueProperty, String legalEntityId, String orgId) {
    //@formatter:off
    String hql = 
               " as t "+
               "   join t.organization as o"+
               " where t."+nameOrValueProperty+" = :value "+
               "   and (o.legalEntityOrganization.id = :legalEntityId "+
               "   or ad_org_isinnaturaltree(o.id, :orgId, o.client.id) = 'Y')";
    //@formatter:on

    return OBDal.getInstance()
        .createQuery(entityName, hql)
        .setNamedParameter("value", nameOrValue)
        .setNamedParameter("legalEntityId", legalEntityId)
        .setNamedParameter("orgId", orgId)
        .setFilterOnActive(false)
        .setFilterOnReadableOrganization(false)
        .setMaxResult(1)
        .uniqueResult() != null;
  }

  private String getNameOrValue(BaseOBObject currentObject) {
    if (currentObject.getEntity().getName().equals(PriceList.ENTITY_NAME)) {
      return PriceList.PROPERTY_NAME;
    }
    return Product.PROPERTY_SEARCHKEY;
  }
}
